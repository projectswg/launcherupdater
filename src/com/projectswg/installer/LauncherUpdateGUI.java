/*
 * 
 * This file is part of ProjectSWG Launchpad.
 *
 * ProjectSWG Launchpad is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * ProjectSWG Launchpad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with ProjectSWG Launchpad.  If not, see <http://www.gnu.org/licenses/>.      
 *
 */
package com.projectswg.installer;

import java.util.concurrent.atomic.AtomicReference;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class LauncherUpdateGUI extends Application {
	
	private static final AtomicReference<LauncherUpdateGUI> INSTANCE = new AtomicReference<>(null);
	
	private final ProgressBar progressBar;
	private final Label progressStatus;
	private final AtomicReference<Stage> primaryStage;
	
	public LauncherUpdateGUI() {
		this.progressBar = new ProgressBar();
		this.progressStatus = new Label("");
		this.primaryStage = new AtomicReference<>(null);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage.set(primaryStage);
		VBox root = new VBox();
		{
			VBox progress = new VBox();
			progress.setFillWidth(true);
			progress.setPrefWidth(700);
			progressBar.setMaxWidth(Double.MAX_VALUE);
			progressStatus.setMaxWidth(Double.MAX_VALUE);
			progress.getChildren().addAll(progressStatus, progressBar);
			root.getChildren().add(progress);
		}
		root.setFillWidth(false);
		root.setAlignment(Pos.CENTER);
		root.setPrefSize(786, 486);
		
		primaryStage.setScene(new Scene(root));
		primaryStage.setTitle("Launcher Updater");
		primaryStage.show();
		LauncherUpdateGUI.INSTANCE.set(this);
	}
	
	public void setProgress(double progress) {
		Platform.runLater(() -> progressBar.setProgress(progress));
	}
	
	public void setStatus(String status) {
		Platform.runLater(() -> progressStatus.setText(status));
	}
	
	public void launch(Application pswg) throws Exception {
		pswg.start(primaryStage.get());
	}
	
	public void close() {
		primaryStage.get().close();
	}
	
	public static LauncherUpdateGUI getInstance() {
		return INSTANCE.get();
	}
	
}
