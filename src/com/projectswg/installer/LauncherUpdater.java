/*
 * 
 * This file is part of ProjectSWG Launchpad.
 *
 * ProjectSWG Launchpad is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * ProjectSWG Launchpad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with ProjectSWG Launchpad.  If not, see <http://www.gnu.org/licenses/>.      
 *
 */
package com.projectswg.installer;

import com.projectswg.common.concurrency.Delay;
import com.projectswg.common.control.IntentManager;
import com.projectswg.common.debug.Log;
import com.projectswg.common.debug.Log.LogLevel;
import com.projectswg.common.debug.log_wrapper.ConsoleLogWrapper;
import com.projectswg.common.javafx.ResourceUtilities;
import com.projectswg.common.process.JarProcessBuilder;
import com.projectswg.common.process.JarProcessBuilder.MemoryUnit;
import com.projectswg.common.utilities.LocalUtilities;
import javafx.application.Platform;

import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;

public class LauncherUpdater {
	
	private static final String LOCAL_JAR_FILE = "Launcher.jar";
	
	public static void main(String [] args) {
		LocalUtilities.setApplicationName(".projectswg/launcher");
		ResourceUtilities.setPrimarySource(LauncherUpdater.class);
		Log.addWrapper(new ConsoleLogWrapper(LogLevel.VERBOSE));
		IntentManager.setInstance(new IntentManager(Runtime.getRuntime().availableProcessors()));
		
		AtomicBoolean running = new AtomicBoolean(true);
		new Thread(() -> guiThread(args, running), "launcher-gui-thread").start();
		while (LauncherUpdateGUI.getInstance() == null) { // takes on average 500ms
			Delay.sleepMilli(50);
		}
		if (!running.get())
			return;
		if (!update(running)) {
			Log.w("Failed update.");
			Delay.sleepSeconds(5);
		}
		if (!running.get())
			return;
		launch();
		System.exit(0);
	}
	
	private static void guiThread(String [] args, AtomicBoolean running) {
		LauncherUpdateGUI.launch(LauncherUpdateGUI.class, args);
		// Hits this point if the updater was exited out of
		running.set(false);
	}
	
	private static boolean update(AtomicBoolean running) {
		LauncherUpdateGUI gui = LauncherUpdateGUI.getInstance();
		return RemoteInstaller.install(running, (progress, status) -> {
			gui.setProgress(progress);
			gui.setStatus(status);
		});
	}
	
	private static boolean launch() {
		try {
			File updaterDirectory = LocalUtilities.getApplicationDirectory();
			File src = new File(updaterDirectory, LOCAL_JAR_FILE);
			JarProcessBuilder builder = new JarProcessBuilder(JarProcessBuilder.getJavaPath(), src).setMemory(256, 256, MemoryUnit.MEGABYTES).inheritIO();
			Log.i("Launching jar... '%s'", src);
			Platform.runLater(LauncherUpdateGUI.getInstance()::close);
			builder.start().waitFor();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
}
