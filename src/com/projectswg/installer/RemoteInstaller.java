/*
 * 
 * This file is part of ProjectSWG Launchpad.
 *
 * ProjectSWG Launchpad is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * ProjectSWG Launchpad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with ProjectSWG Launchpad.  If not, see <http://www.gnu.org/licenses/>.      
 *
 */
package com.projectswg.installer;

import com.projectswg.common.concurrency.Delay;
import com.projectswg.common.debug.Assert;
import com.projectswg.common.debug.Log;
import com.projectswg.common.utilities.LocalUtilities;
import me.joshlarson.json.JSON;
import me.joshlarson.json.JSONException;
import me.joshlarson.json.JSONObject;
import me.joshlarson.json.JSONOutputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class RemoteInstaller {
	
	private static final String MANIFEST			= "manifest.json";
	private static final String INSTALLER			= "ProjectSWG.jar";
	private static final String LAUNCHER			= "Launcher.jar";
	private static final String LAUNCHER_GAME		= "GameLauncher.jar";
	private static final String DATA				= "data.zip";
	
	private static final String REMOTE_URL			= "patch1.projectswg.com";
	private static final String REMOTE_PATH			= "launcher_patch";
	private static final String REMOTE_MANIFEST		= REMOTE_PATH+'/'+MANIFEST;
	private static final String REMOTE_INSTALLER	= REMOTE_PATH+'/'+INSTALLER;
	private static final String REMOTE_LAUNCHER		= REMOTE_PATH+'/'+LAUNCHER;
	private static final String REMOTE_LAUNCHER_GAME= REMOTE_PATH+'/'+LAUNCHER_GAME;
	private static final String REMOTE_DATA			= REMOTE_PATH+'/'+DATA;
	
	private final InstallStatusCallback callback;
	private final File updateDirectory;
	private final AtomicReference<InstallStatus> status;
	private final AtomicReference<String> installerVersion;
	private final AtomicReference<String> launcherVersion;
	private final AtomicReference<String> remoteInstallerVersion;
	private final AtomicReference<String> remoteLauncherVersion;
	private final AtomicBoolean updateInstaller;
	private final AtomicBoolean updateLauncher;
	
	private RemoteInstaller(InstallStatusCallback callback) {
		this.callback = callback;
		this.updateDirectory = LocalUtilities.getApplicationDirectory();
		this.status = new AtomicReference<>(InstallStatus.CREATED);
		this.installerVersion = new AtomicReference<>(null);
		this.launcherVersion = new AtomicReference<>(null);
		this.remoteInstallerVersion = new AtomicReference<>(null);
		this.remoteLauncherVersion = new AtomicReference<>(null);
		this.updateInstaller = new AtomicBoolean(false);
		this.updateLauncher = new AtomicBoolean(false);
	}
	
	private void initialize() throws InstallerException {
		verifyStatus(InstallStatus.CREATED, InstallStatus.INITIALIZED);
		try {
			File manifest = new File(updateDirectory, MANIFEST);
			if (!manifest.isFile()) {
				Log.d("  Manifest file missing");
				return;
			}
			
			JSONObject obj = JSON.readObject(new FileInputStream(manifest), true);
			if (obj == null || !obj.containsKey("installerVersion") || !obj.containsKey("launcherVersion")) {
				Log.d("  Invalid local manifest file. %s", obj);
				return;
			}
			
			installerVersion.set(obj.getString("installerVersion"));
			launcherVersion.set(obj.getString("launcherVersion"));
			Log.d("  Installer Version: %s", installerVersion.get());
			Log.d("  Launcher Version:  %s", launcherVersion.get());
		} catch (IOException e) {
			Log.e(e);
			throw new InstallerException("initialize", e);
		}
	}
	
	private boolean testConnection() {
		verifyStatus(InstallStatus.INITIALIZED, InstallStatus.CONNECTED);
		try {
			RemoteUtilities.testConnection(REMOTE_URL, REMOTE_MANIFEST);
			Log.d("  Valid connection.");
			return true;
		} catch (IOException e) {
			Log.e(e);
			return false;
		}
	}
	
	private void queryUpdates() throws InstallerException {
		verifyStatus(InstallStatus.CONNECTED, InstallStatus.QUERY_UPDATES);
		updateStatus(0, "Querying updates...");
		try {
			JSONObject obj = JSON.readObject(RemoteUtilities.fetch(REMOTE_URL, REMOTE_MANIFEST));
			if (obj == null || !obj.containsKey("installerVersion") || !obj.containsKey("launcherVersion"))
				throw new InstallerException("query updates", "Invalid remote manifest");
			
			if (installerVersion.get() == null || launcherVersion.get() == null) {
				Log.d("  Downloading installer and launcher by default.");
				updateInstaller.set(true);
				updateLauncher.set(true);
				return;
			}
			
			remoteInstallerVersion.set(obj.getString("installerVersion"));
			remoteLauncherVersion.set(obj.getString("launcherVersion"));
			updateInstaller.set(installerVersion.get().compareTo(obj.getString("installerVersion")) < 0);
			updateLauncher.set(launcherVersion.get().compareTo(obj.getString("launcherVersion")) < 0);
			updateLauncher.set(updateLauncher.get() && !updateInstaller.get());
			Log.d("  Update Installer: %b", updateInstaller.get());
			Log.d("  Update Launcher:  %b", updateLauncher.get());
		} catch (JSONException | IOException e) {
			Log.e(e);
			throw new InstallerException("query updates", e);
		}
	}
	
	private void downloadUpdates() throws InstallerException {
		verifyStatus(InstallStatus.QUERY_UPDATES, InstallStatus.DOWNLOADING);
		if (updateInstaller.get()) {
			downloadInstallerUpdates();
		}
		if (updateLauncher.get()) {
			downloadLauncherUpdates();
		}
	}
	
	private void downloadInstallerUpdates() throws InstallerException {
		updateStatus(.2, "Starting installer download...");
		try {
			Log.d("  Downloading installer...");
			File runningJar;
			File downloadLocation;
			try {
				URI uri = RemoteInstaller.class.getProtectionDomain().getCodeSource().getLocation().toURI();
				runningJar = new File(uri.getPath());
				downloadLocation = new File(uri.getPath() + ".part");
			} catch (URISyntaxException e) {
				Log.e(e);
				runningJar = new File(INSTALLER);
				downloadLocation = new File(INSTALLER + ".part");
			}
			download(REMOTE_INSTALLER, downloadLocation, 0.2, 0.8, "Downloading installer patch...");
			installerVersion.set(remoteInstallerVersion.get());
			
			final File runningJarFinal = runningJar;
			final File downloadLocationFinal = downloadLocation;
			Runtime.getRuntime().addShutdownHook(new Thread(() -> {
				try {
					Files.move(downloadLocationFinal.toPath(), runningJarFinal.toPath(), StandardCopyOption.REPLACE_EXISTING);
				} catch (IOException e) {
					Log.e(e);
				}
			}));
		} catch (IOException e) {
			Log.e(e);
			throw new InstallerException("downloading installer updates", e);
		}
	}
	
	private void downloadLauncherUpdates() throws InstallerException {
		updateStatus(.2, "Starting launcher download...");
		try {
			double basePercent = 0.2;
			double totalPercent = ((int) (0.8 / 3 * 100)) / 100.0;
			Log.d("  Downloading launcher...");
			download(REMOTE_LAUNCHER, new File(updateDirectory, LAUNCHER), basePercent, totalPercent, "Downloading launcher patch [core]...");
			basePercent += totalPercent;
			download(REMOTE_LAUNCHER_GAME, new File(updateDirectory, LAUNCHER_GAME), basePercent, totalPercent, "Downloading launcher patch [game]...");
			basePercent += totalPercent;
			Log.d("  Downloading data...");
			download(REMOTE_DATA, new File(updateDirectory, DATA), basePercent, totalPercent, "Downloading data patch...");
			launcherVersion.set(remoteLauncherVersion.get());
		} catch (IOException e) {
			Log.e(e);
			throw new InstallerException("downloading installer updates", e);
		}
	}
	
	private void completeDownload(AtomicBoolean running) throws InstallerException {
		try (JSONOutputStream output = new JSONOutputStream(new FileOutputStream(new File(updateDirectory, MANIFEST)))) {
			JSONObject obj = new JSONObject();
			obj.put("installerVersion", installerVersion.get());
			obj.put("launcherVersion", launcherVersion.get());
			output.writeObject(obj);
		} catch (IOException e) {
			Log.e(e);
			throw new InstallerException("complete download-update manifest", e);
		}
		if (updateLauncher.get()) {
			completeLauncherDownload();
		}
		if (updateInstaller.get()) {
			completeInstallerDownload(running);
		}
	}
	
	private void completeLauncherDownload() throws InstallerException {
		Log.d("  Unzipping data...");
		File data = new File(updateDirectory, DATA);
		if (!data.isFile()) {
			Log.w("    Cannot unzip data.zip - doesn't exist");
			throw new InstallerException("unzipping data", "data.zip did not download properly");
		}
		try (ZipInputStream zis = new ZipInputStream(new FileInputStream(data))) {
			for (ZipEntry ze = zis.getNextEntry(); ze != null; ze = zis.getNextEntry()) {
				File newFile = new File(updateDirectory, ze.getName());
				Log.d("    Inflating %s...", newFile);
				if (ze.isDirectory()) {
					if (!newFile.isDirectory() && newFile.mkdirs())
						Log.w("    Couldn't create directory %s!  Attempting to proceed...", newFile);
				} else {
					try (FileOutputStream out = new FileOutputStream(newFile)) {
						byte [] buffer = new byte[4096];
						int read = zis.read(buffer);
						while (read > 0) {
							out.write(buffer, 0, read);
							read = zis.read(buffer);
						}
					}
				}
				zis.closeEntry();
			}
			zis.close();
		} catch (IOException e) {
			Log.e(e);
			throw new InstallerException("unzipping data", e);
		} finally {
			if (!data.delete())
				Log.w("    Failed to delete the downloaded 'data.zip'!");
		}
	}
	
	private void completeInstallerDownload(AtomicBoolean running) {
		Log.d("  Terminal operation. Waiting for user to restart launcher.");
		updateStatus(1, "Please restart the launcher to complete the update.");
		while (running.get()) { // Wait for user to restart the launcher
			Delay.sleepSeconds(1);
		}
	}
	
	private void updateStatus(double progress, String status) {
		this.callback.update(progress, status);
	}
	
	private void verifyStatus(InstallStatus oldStatus, InstallStatus newStatus) {
		Log.d("updateStatus(): %s -> %s", oldStatus, newStatus);
		Assert.test(status.compareAndSet(oldStatus, newStatus), "Unable to update status! Expected: " + oldStatus);
	}
	
	private void download(String remotePath, File local, double basePercent, double totalPercent, String status) throws IOException {
		RemoteUtilities.download(REMOTE_URL, remotePath, local, (download, percent) -> updateStatus(basePercent+percent*totalPercent, status));
	}
	
	/**
	 * Safe function to reinstall the application. All failures are handled
	 * @param callback the callback for progress updates
	 * @return TRUE if the launcher was successfully updated, FALSE otherwise
	 */
	public static boolean install(AtomicBoolean running, InstallStatusCallback callback) {
		try {
			RemoteInstaller installer = new RemoteInstaller(callback);
			if (!running.get())
				return true;
			installer.initialize();
			if (!running.get())
				return true;
			if (!installer.testConnection())
				return true;
			if (!running.get())
				return true;
			installer.queryUpdates();
			if (!running.get())
				return true;
			installer.downloadUpdates();
			if (!running.get())
				return true;
			installer.completeDownload(running);
			if (!running.get())
				return true;
			installer.verifyStatus(InstallStatus.DOWNLOADING, InstallStatus.DONE);
			return true;
		} catch (InstallerException e) {
			callback.update(-1, String.format("Failed '%s'. %s", e.getOperation(), e.getMessage()));
			return false;
		} catch (Throwable t) {
			callback.update(-1, "Failed. " + t.getClass().getSimpleName() + ": " + t.getMessage());
			Log.e(t);
			return false;
		}
	}
	
	public interface InstallStatusCallback {
		void update(double progress, String status);
	}
	
	private static class InstallerException extends Exception {
		
		private static final long serialVersionUID = 1L;
		
		private final String operation;
		
		public InstallerException(String operation, String message) {
			super(message);
			this.operation = operation;
		}
		
		public InstallerException(String operation, Throwable cause) {
			this(operation, cause.getClass().getSimpleName() + ": " + cause.getMessage());
		}
		
		public String getOperation() {
			return operation;
		}
		
	}
	
	private enum InstallStatus {
		CREATED,
		INITIALIZED,
		CONNECTED,
		QUERY_UPDATES,
		DOWNLOADING,
		DONE
	}
	
}
