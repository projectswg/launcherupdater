/*
 * 
 * This file is part of ProjectSWG Launchpad.
 *
 * ProjectSWG Launchpad is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * ProjectSWG Launchpad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with ProjectSWG Launchpad.  If not, see <http://www.gnu.org/licenses/>.      
 *
 */
package com.projectswg.installer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.atomic.AtomicLong;

public class RemoteUtilities {
	
	public static void testConnection(String host, String path) throws IOException {
		try (InputStream is = setupConnection(host, path, new AtomicLong())) {
			
		}
	}
	
	public static void download(String host, String path, File localPath, DownloadCallback callback) throws IOException {
		byte [] buffer = new byte[16 * 1024];
		AtomicLong expectedLength = new AtomicLong(-1);
		try (InputStream is = setupConnection(host, path, expectedLength)) {
			try (FileOutputStream fos = new FileOutputStream(localPath)) {
				transfer(buffer, expectedLength.get(), is, fos, callback);
				fos.flush();
			}
		}
	}
	
	public static String fetch(String host, String path) throws IOException {
		byte [] buffer = new byte[16 * 1024];
		AtomicLong expectedLength = new AtomicLong(-1);
		try (InputStream is = setupConnection(host, path, expectedLength)) {
			try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
				transfer(buffer, expectedLength.get(), is, baos, null);
				baos.flush();
				return baos.toString("ASCII");
			}
		}
	}
	
	private static void transfer(byte [] buffer, long expectedLength, InputStream is, OutputStream os, DownloadCallback callback) throws IOException {
		int bytesRead = 0;
		long downloaded = 0;
		while ((bytesRead = is.read(buffer)) > -1) {
			os.write(buffer, 0, bytesRead);
			downloaded += bytesRead;
			if (callback != null)
				callback.onDownloaded(downloaded, downloaded / (double) expectedLength);
		}
	}
	
	private static InputStream setupConnection(String host, String path, AtomicLong expectedLength) throws IOException {
		if (!path.startsWith("/"))
			path = '/' + path;
		URLConnection urlConnection = new URL("http", host, path).openConnection();
		urlConnection.connect();
		expectedLength.set(urlConnection.getContentLength());
		return urlConnection.getInputStream();
	}
	
	public interface DownloadCallback {
		void onDownloaded(long downloaded, double percentage);
	}
	
}
